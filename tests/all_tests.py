import base_test

class TestCases(base_test.BaseTest):

    def test_01_test_login_page_login(self):
        assert self.welcomepage.click_login(self.loginpage)
        assert self.loginpage.login(self.mainpage,"testuser","testing")
        assert self.mainpage.dropdown_add_numbers(self.mainpage,4,4)
