import time
from page_objects import PageObject, PageElement
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import By



class MainPage(PageObject):

    number_1_select = PageElement(id_ = "num1")
    number_2_select = PageElement(id_ = "num2")

    dropdown_submit_btn = PageElement(id_ = "btn-success")
    dropdown_result = PageElement(id_ = "addResult")

    def check_page(self):
        return "Logged In" in self.w.title

    def dropdown_add_numbers(self, num1,num2, result):
        selectnum1 = Select(self.number_1_select)
        selectnum2 = Select(self.number_2_select)

        waitForSelect = WebDriverWait(self.w,10).until(EC._element_if_visible(By.self.number_1_select))
        selectnum1.select_by_value(str(num1))
        selectnum2.select_by_value(str(num2))
        self.dropdown_submit_btn.click()
        time.sleep(1)
        return str(result) in self.dropdown_result.text

